#!/usr/bin/python
"""
========================
========================
AI-Snooper
========================
========================

Author: Gananath.R
Contact: https://github.com/Gananath

"""

import os
import pyscreenshot as ImageGrab
from datetime import datetime
from os.path import expanduser
import smtplib
import Image
import socket
import httplib, urllib, base64
import glob
import json 
import time

###>>>>>>Change Parameteres Inside this Block <<<<<<<<<<<<###

#Set the path for the hidden folder where you want to save the data 
#Always end the file path with a slash "/"

PATH="/home/gananath/.AI-Snooper/"
#PATH=expanduser("~")+"/.AI-Snooper/" 

#Enter your E-MAIL address to forward the notifications
TO_EMAIL="AI-Snooper@grr.la"

#Default "From" Email. Please keep this as such
FROM_EMAIL="noreply@ai-snooper.com"

#Minimum Interval time in minutes required for Free Microsoft Computer Vision API
TIME=20

#Microsoft Computer vision API KEY
KEY='sdfw35df32e2356w564452g322e' #Remove this key and add your key

###>>>>>>>>>>>>>>>>>End of Block<<<<<<<<<<<<<<<###

tm=str(datetime.now())

def take_screenshot(low_resolution=True):
    size = 512, 512
    # fullscreen
    try:
        im = ImageGrab.grab()
        if low_resolution==True:
            im.thumbnail(size, Image.ANTIALIAS) #resizing the image
        im.save(PATH+tm+".png")
        
    except:
        print "Error while grabbing screenshots"

def write_output(api_result):
    file_name=PATH+"Snoopy-log.txt"
    if os.path.exists(file_name):
        append_write = 'a' # append if already exists
    else:
        append_write = 'w' # make a new file if not
        
    
    txt_file=open(file_name,append_write)
    txt_file.write("Time: "+tm+"\n"+str(api_result)+"\n")
    txt_file.close()
    
def send_Mail(adult_result,description):
    #NEED SMTP SERVER INSTALLED, Check for SMTP in your computer else install manually
    #sudo apt-get install postfix
    sender = FROM_EMAIL
    receivers = [TO_EMAIL]
    SUBJECT="AI-Snooper notification Mail"
    TEXT = """From: From Person <"""+str(FROM_EMAIL)+""">
             To: To Person <"""+str(TO_EMAIL)+""">
              Subject: """+str(SUBJECT)+"""
               Snooper Notification \n
               Time = """+str(tm)+"""\n
               Adult Content = """+str(adult_result)+"""\n
               Description = """+str(description)+"""\n 
               
               This is a auto generated email.Please dont reply to this email.
                 
                """
    message = 'Subject: {}\n\n{}'.format(SUBJECT, TEXT)
    try:
        smtpObj = smtplib.SMTP('localhost')
        smtpObj.sendmail(sender, receivers, message)         
        #print "Successfully sent email"
    except SMTPException:
        print "Error: unable to send email"

def is_connected():
    try:
        # connect to the host -- tells us if the host is actually
        # reachable
        socket.create_connection(("www.google.com", 80))
        return True
    except OSError:
        pass
    return False
    
def MicrosoftVisionAPI():
    headers = {
         # Request headers. Replace the key below with your subscription key.
        'Content-Type': 'application/octet-stream',
        'Ocp-Apim-Subscription-Key':KEY ,
        }
    params = urllib.urlencode({
        # Request parameters. All of them are optional.
        'visualFeatures': 'Categories,Tags,Description,Adult',
        'language': 'en',
        })
    # Replace the three dots below with the URL of a JPEG image of a celebrity.
    
    pathToFile = glob.glob(PATH+"*.png")[0]
    with open( pathToFile, 'rb' ) as f:
        body = f.read()
    try:
        conn = httplib.HTTPSConnection('westus.api.cognitive.microsoft.com')
        conn.request("POST", "/vision/v1.0/analyze?%s" % params, body, headers)
        response = conn.getresponse()
        data = response.read()
        conn.close()
       
    except Exception as e:
        print("[Errno {0}] {1}".format(e.errno, e.strerror))
    
    os.remove(pathToFile) #removing imge file
    return data
    

def parse_json(json_result):
    j_obj=json.loads(json_result)
    return j_obj

def main():
    if is_connected()==True and PATH[-1:]=="/":
        time.sleep(1*60) #
        take_screenshot()
        time.sleep(2*60)
        api_call=MicrosoftVisionAPI()
        write_output(api_call)
        time.sleep(2*60)
        j=parse_json(api_call)
        is_adult=j["adult"]["isAdultContent"]
        if is_adult==True:
            desc=j["description"]
            send_Mail(is_adult,desc)
            time.sleep(2*60)
        time.sleep(TIME*60-2*60-2*60-2*60-1*60)
    else:
        print "Check your Internet ConnectionPA or File Path"
    
if __name__ == '__main__':
    while True:
        main()
