# AI-Snooper
Monitoring, Logging and Email notification of desktop activity.


# Requirements 
- Python 2.7

- SMTP-Server 
```
sudo apt-get install mailutils
sudo apt-get install postfix
```

- pyscreenshot==0.4.2

- json==2.0.9

- [Microsoft's Computer Vision API](https://www.microsoft.com/cognitive-services/en-us/computer-vision-api)

# How it Works
It takes screenshot of your desktop and send that image to *Microsoft's Computer vision API* and fetchs the vision features for that screenshot. Currently in this initial version of *AI-Snooper* we are only going to fetch the parameters for classifing adult images but it can do much more. If the result for the Adult image classification is true then the script will send an email to your personal address with some meta data. I tried to make the code very portable so that any one can reuse it in their own personal projects.

# How to use it
You have to just change the parameters inside this block

```
###>>>>>>Change Parameteres Inside this Block <<<<<<<<<<<<###

#Set the path for the hidden folder where you want to save the data 
#Always end the file path with a slash "/"

PATH="/home/gananath/.AI-Snooper/"
#PATH=expanduser("~")+"/.AI-Snooper/" 

#Enter your E-MAIL address to forward the notifications
TO_EMAIL="AI-Snooper@grr.la"

#Default "From" Email. Please keep this as such
FROM_EMAIL="noreply@ai-snooper.com"

#Minimum Interval time in minutes required for Free Microsoft Computer Vision API
TIME=20

#Microsoft Computer vision API KEY
KEY='sdfw35df32e2356w564452g322e'

###>>>>>>>>>>>>>>>>>End of Block<<<<<<<<<<<<<<<###
```

# Advanced
Inorder to make the script self sufficient and for automation now I am using `sleep()` to execute the script periodically. But if you have enough experience I would recommend using `cron` jobs instead for `sleep()`. If you look at the code I have added many `sleep()` and its because to prevent any sudden spike in memory or CPU usage while executing the script.

If you want to keep the screen shots then hide `os.remove(pathToFile) #removing imge file` but I have to warn you it may cause some problem in the execution.
